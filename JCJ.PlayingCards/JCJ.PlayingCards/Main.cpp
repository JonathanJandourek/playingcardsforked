#include <iostream>
#include <conio.h>

using namespace std;

void PrintCard(struct Card &card);
Card HighCard(struct Card &card1, struct Card &card2);

enum Rank
{
	ONE = 1,
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

enum Suit
{
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	Card card;
	Card card1;
	Card card2;
	card.rank = TWO;
	card.suit = DIAMONDS;

	PrintCard(card);
	HighCard(card1, card2);

	_getch();
	return 0;
}

//BOTH FUNCTIONS RETURN THE INT AND NOT THE ENUMERATION NAME
void PrintCard(struct Card &card)
{
	card.rank = TWO;
	card.suit = DIAMONDS;
	std::cout << card.rank << " of " << card.suit;
}

Card HighCard(struct Card &card1,struct Card &card2)
{
	card1.rank = ACE;
	card1.suit = SPADES;

	card2.rank = QUEEN;
	card2.suit = DIAMONDS;
	if(card1.rank>card2.rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}

}